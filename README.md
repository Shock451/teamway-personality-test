# Personality Quiz

[![Netlify Status](https://api.netlify.com/api/v1/badges/73832388-6e76-4690-a935-6e21bf3eeec6/deploy-status)](https://app.netlify.com/sites/ao-personality-quiz/deploys)

The Personality Quiz application takes 3 to 5 different questions, maps them into a score, and produces a verdict of either Introvert or Extrovert.

![Quiz Screen](/screenshots/Quiz%20Screen.png)

## Getting Started

These instructions will get you a copy of this project up and running on your local machine for development and testing purposes.

### Installing

- Open your local development terminal
- `cd` into the directory that you want the project to reside e.g:

```
cd projects
```

- Clone the repository into that directory
Edit this to 
```
git clone https://gitlab.com/Shock451/teamway-personality-test.git
```
- Run `yarn` to install the project dependencies
- Run `yarn start` to start a local development server
- Run `yarn test` to run unit tests
- Navigate to http://localhost:3000 to view the project

## Live Demo

A fully functional demo of this project is hosted on Netlify and is available here: https://ao-personality-quiz.netlify.app/

## App Features

- A Quiz to determine if user is an Introvert or Extrovert
- Mobile responsive

## Built With

- [React.js](https://www.reactjs.org) - Javascript library for building user interfaces
- [Chakra UI](https://www.chakra-ui.com) - A simple component library for building React apps
- [Netlify](https://www.netlify.com) - For easy deployment of web apps