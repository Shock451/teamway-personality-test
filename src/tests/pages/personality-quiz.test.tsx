import { render, screen } from "@testing-library/react";
import { QuizControls } from "../../pages/personality-quiz/components";

function NotImplemented() {
  throw new Error("Function not implemented.");
}

describe("QuizControls", () => {
  it("renders a disabled prev button on the first question", () => {
    render(
      <QuizControls
        onClickSubmit={NotImplemented}
        onClickPrev={NotImplemented}
        onClickNext={NotImplemented}
        isNextDisabled={false}
        isLastPage={false}
        canSubmit={false}
        page={0}
      />
    );
    const prevButton = screen.getByRole("button", { name: /prev/i }) as HTMLButtonElement;
    expect(prevButton.disabled).toEqual(true);
  });
});
