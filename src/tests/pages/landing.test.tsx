import Enzyme, { shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Landing } from "../../pages";

Enzyme.configure({ adapter: new Adapter() });

describe("Landing", () => {
  test("renders learn react link", () => {
    const page = shallow(<Landing />);
    const buttonElement = page.find("#take-quiz-button");
    expect(buttonElement.text()).toEqual("Take quiz!");
  });
});
