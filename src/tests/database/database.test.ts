import { Database } from "../../database";

describe("Database", () => {
  it("throws an error when fetching a negative question number", () => {
    const db = new Database()
    expect(() => db.fetchQuestion(-4)).toThrowError();
  });

  it("throws an error when fetching an index out of bounds", () => {
    const db = new Database()
    const totalQuestions = db.countQuestions();
    expect(() => db.fetchQuestion(totalQuestions)).toThrowError();
  });
});
