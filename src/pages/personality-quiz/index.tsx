import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Center, Text } from "@chakra-ui/react";
import config from "../../config";
import { Database } from "../../database";
import { calculateVerdict, getRandomIndices } from "../../utils";
import { IOption } from "../../interfaces/option";
import { IQuestion } from "../../interfaces/question";
import { OptionList, QuizControls } from "./components";

const { questionsPerQuiz } = config;
const db = new Database();
const totalQuestions: number = db.countQuestions();

function PersonalityTest() {
  const navigate = useNavigate();
  const [page, setPage] = useState<number>(0);
  const [answers, setAnswers] = useState<number[]>(new Array(questionsPerQuiz));
  const [questionIds] = useState<number[]>(getRandomIndices(
    questionsPerQuiz,
    totalQuestions
  ));
  const currentQuestion: IQuestion = db.fetchQuestion(questionIds[page]);

  const isLastPage = page >= questionsPerQuiz - 1;
  const isNextDisabled = !answers[page] || isLastPage;
  const canSubmit = isLastPage && !!answers[questionsPerQuiz - 1];

  const handleSelect = (option: IOption) => {
    let temp = [...answers];
    temp[page] = option.id;
    setAnswers(temp);
  };

  const handlePrev = () => {
    setPage((prevState) => prevState - 1);
  };

  const handleNext = () => {
    setPage((prevSate) => prevSate + 1);
  };

  const handleSubmit = () => {
    const verdict = calculateVerdict(answers);

    navigate("/result", {
      state: {
        verdict,
      },
    });
  };

  return (
    <Center
      minH={667}
      w="100vw"
      h="calc(100% - 50px)"
      bgGradient={[
        "linear(to-tr, teal.300, yellow.400)",
        "linear(to-t, blue.200, teal.500)",
        "linear(to-b, orange.100, purple.300)",
      ]}
    >
      <Box w={600} rounded={4} shadow="md" p={4} bgColor="#f5f7f9">
        <Text fontSize="smaller" borderColor="gray.500" fontWeight="light">
          Question {page + 1}/{questionsPerQuiz}
        </Text>
        <Text my={2} fontSize="lg" fontWeight="bold">
          {currentQuestion.question}
        </Text>
        <OptionList
          answer={answers[page]}
          options={currentQuestion.options}
          questionId={currentQuestion.id}
          onSelect={handleSelect}
        />
        <QuizControls
          onClickPrev={handlePrev}
          onClickNext={handleNext}
          isLastPage={isLastPage}
          canSubmit={canSubmit}
          isNextDisabled={isNextDisabled}
          onClickSubmit={handleSubmit}
          page={page}
        />
      </Box>
    </Center>
  );
}

export { PersonalityTest };
