import { IQuestion } from "../interfaces/question";
import Data from "./questions.json";

class Database {
  private _questions: IQuestion[];

  constructor() {
    this._questions = Data.questions;
  }

  countQuestions = (): number => {
    return this._questions.length;
  };

  fetchQuestion = (idx: number): IQuestion => {
    if (idx < 0) {
      throw new Error("Invalid request: question index cannot be negative");
    }
    if (idx >= this._questions.length) {
      throw new Error("Invalid request: question index is out of bounds");
    }
    return this._questions[idx];
  };
}

export { Database };
